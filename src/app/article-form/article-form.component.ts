import { Router } from '@angular/router';
import { ImageService } from './../image.service';
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-article-form',
  templateUrl: './article-form.component.html',
  styleUrls: ['./article-form.component.css']
})
export class ArticleFormComponent implements OnInit {

  url:string;
  text:string;
  doc:string;

  constructor(private classifyService:ClassifyService, 
              public imageService:ImageService,
              private router:Router ) { }

  ngOnInit() 
  {
  }

  docFormSubmit()
  {
    this.classifyService.doc = this.text;
    this.doc = this.text;
    this.router.navigate(['/classified-article']);
  }


}
