import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit 
{

  email:string;
  password:string;

  constructor(private authService: AuthService,
              private router:Router,
              ) 
              { }

  ngOnInit() 
  {
  }
  
  
  Login()
  {
    console.log('In login.ts- signIn()');
    this.authService.Login(this.email,this.password);
  }




}
